---
title: Coraline-DataScientist
aliases: []
tags: [job_description]
---

orgainisation: [[Coraline]]
position: [[Data Scientist]]
created: [[2021-07-12]]

**Responsibilities**

-   Working with cutting-edge data-driven solutions to client problems
-   Design and implement predictive statistical, [[machine learning]], and optimization models to solve business problems.
-   Developing, evaluating, and deploying new algorithms and methods
-   Understanding clients’ processes and communicating your solutions and results to stakeholders and clients.

**Qualifications**

-   Bachelor or Master Degree in Mathematics, Computer Science, Information Management Statistics or equivalent industry experience.
	-   [[Apr 2020 - I graduated with a Bachelor in Comptuter Engineering from KMUTNB]]
-   Knowledge of a variety of [[machine learning]] techniques (clustering, decision tree learning, artificial neural networks, etc.) and their real-world advantages/drawbacks.
	-   [[Feb 2021 - I achieved TensorFlow Developer Certificate]]
	-   [[I implemented simple machine learning models]]
	-   [[Knowledge Discovery and Data Mining (Course)|Data Mining (course)]]
	-   [[Artificial Intelligence (Course)|AI(course)]]
-   Knowledge of advanced statistical techniques and concepts (regression, properties of distributions, statistical tests, and proper usage, etc.) and experience with applications.
	-   [[climate trend analysis]]
	-   [[Senior Project - Bias Correction of Multi-Dimensional Climate Data and Visualisation]]
	-   [[Statistics for Computer Engineer]]
	-   [[Mann-Kendal test]] and [[Theil-Sen Estimator]]
-   Experience in using statistical computer languages ( [[Python]], [[SQL]], etc.) to manipulate data and draw insights from large data sets
	-   [[Senior Project - Bias Correction of Multi-Dimensional Climate Data and Visualisation]]
	-   [[I have 4+ years of Python experience]]
	-   [[Database (Course)]]
-   A strong passion to identify and solve business problems using [[data science]] techniques
-   Troubleshooting and analytical skills
-   Good communication, storytelling, presentation, and collaboration skills
-   An outgoing personality who has proven to be a good team player
-   Agile methodology mindset 

**Welfare & Benefits:**

-   10 days vacation leave per year
-   Flexible Working Time
-   Five days working ( Monday - Friday)
-   Happy Hours (Free Snacks & Drinks)
-   Project Success Celebration
-   Team building Activity and Company Outing
-   Training and Development
-   Career Opportunity
-   Performance bonus and Annual salary increment
-   Health insurance and Annual physical check

Interested in joining the “Coraline Team”, please send your recently updated CV in English with your portfolio to career@coraline.co.th. If you have any questions, please feel free to call us at 094-440-7451.