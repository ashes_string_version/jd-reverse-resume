---
title: SCB-MLEngineer
aliases: []
tags: [job_description]
---

orgainisation: [[SCB]]
position: [[Machine Learning Engineer]]
created: [[2021-07-12]]

https://careers.scb.co.th/th/jobs/job-types/permanent/machine-learning-engineer

**Job Summary**

As a [[Machine Learning Engineer]] in [[SCB]] Data Analytics you will join a group of enthusiastic people working passionately to leverage the wealth of data for business users. You will build, set up and deliver full-fledged Big Data solutions for data-science and advanced analytics in support of identifyingopportunities and driving business results with a data-driven research approach. 

**Job Description**

Deliver functionality required for business and data analysts, [[Data Scientist|data scientists]] and other business roles to advance the overall analytic performance and strategy of the bank

Join forces with team-mates and other data solutions development divisions in assisting data solutions delivery to implement the data-platform product roadmap 

Develop and maintain data integration processes that are part of the bank’s data platform, whether building data pipelines or micro services or tools 

Implement and automate optimized data pipelines, ETL solutions, model data stores according to internal business, external business and product requirements

Build best practices and strategies for the data platform to achieve data analytics and utilization milestones as per business and management roadmap

Collaborate with data science and create reusable data-assets in order to automate and speed up machine learning model development

Deliver full blown productionized solutions employing mature production-ready machine learning models 

Ensure data-assets are organized and stored in most efficient way so that all information is readily available for easy access and retrieval 

**Qualifications**

BSc or MSc degree in statistics, mathematics, quantitative analysis, computer science, computer engineering, software engineering or information technology

- [[Apr 2020 - I graduated with a Bachelor in Comptuter Engineering from KMUTNB]]

Interest in relevant experience with scripting, developing, debugging and using Big Data technologies (e.g. Hadoop, Spark, Flink, Kafka), respectively working with [[SQL]]/[[NoSQL]]/Graph data store technologies

- [[Senior Project - Bias Correction of Multi-Dimensional Climate Data and Visualisation]]
- [[Simple POS Web Application]]

Desire in getting experience with functional/trait-oriented languages 

Willingness to develop a mature problem-solving mindset 

Eagerness to gain a deep understanding of Information Security principles and discipline

Having a clear understanding of Agile principles, practices and Scrum methodologies 

Has the know-how and the scripting and coding prowess to set up, configure & maintain a machine learning model development environment 

Experience architecting, coding and delivering high performance micro services and/or recommenders delivering recommendations to (tens of) millions of users