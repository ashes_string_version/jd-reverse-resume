---
title: Backyard-Data Scientist (Senior Level)
aliases: []
tags: [job_description]
---

orgainisation: [[Backyard]]
position: [[Data Scientist]] (Senior Level)
last update: [[2021-07-18]]

https://jobs.blognone.com/company/backyard/job/data-scientist-seni-H4IB

Senior [[Data Scientist]] position is accountable for transforming business requirements into plausible actions with appropriate instruments including algorithms, methodology and technological frameworks. This role will perform data analysis in several industries such as healthcare, manufacturing or retail. Since each domain requires different approaches to tackle and solve the issues, the senior data scientist is encouraged to plan, execute and monitor any required actions. In addition, this role will work closely with data scientists and data engineering team to process and analyze the relevant information. 

**ESSENTIAL FUNCTIONS** **:**

· Gather and analyze all types of business requirements which are clear, vague and unidentified and be able to decompose the business requirements into plausible actions for data science team 

· Hands-on experience building statistical models and performs advanced quantitative analyses and model development to drive decision-making to solve complex forecasting and/or ricing/revenue/resource optimization problems. 

· Passion for and ability to conceptualize, develop, and deploy new technological applications and decision support tools, including programming models and web applications in Python and working with developer teams to transform models/applications from prototype to production stage. 

· Familiar with principles of data engineering and working directly with data engineering teams on both cloud based and on premise architecture to support the needs of data science team members 

· Continuous pursuit of cutting-edge knowledge and skills in the fields of statistics, computer science, data science and data engineering. 

 **Qualification:** 

· Bachelor or Master degree in Computer Engineering, Computer Science, Mathematics, Economics and related fields 

- [[Apr 2020 - I graduated with a Bachelor in Comptuter Engineering from KMUTNB]]
- [[Bachelor in Computer Engineering, GPAX 3.83, 1st class honours]]

· Good command in English language is advantageous 

· Minimum 2 years of graduate-level or 3 years of professional experience in a statistics, quantitative modeling, econometrics or data scientist role required. 

- [[Statistics for Computer Engineer]]

· Familiar with common statistical and data science toolkit including Scikit-learn, numpy, scipy, pandas and matplotlib are mandatory 

- [[I have 4+ years of Python experience]]
- [[Feb 2021 - I achieved TensorFlow Developer Certificate]]
- [[Senior Project - Bias Correction of Multi-Dimensional Climate Data and Visualisation]]
- [[Jul 2020 - I achieved a certificate from Andrew Ng's Deep Learning Course(Coursera)]]

· Good command of coding in Python including standard practice, language core functionality and adaptation into advance features 

· Ability to develop the test case for model and algorithms implementation and deployment are highly beneficial 

· Extra knowledge at least one area such as Natural Language Processing, Deep Learning, Knowledge Graph or Ontology are required 

- [[2020 - I am interested in...]]
- [[Feb 2021 - I achieved TensorFlow Developer Certificate]]
- [[Analysis and Visualisation of Thailand Twitter IO Dataset]]

· Good portfolio in ML project in GitHub or public repository is a mandatory 

- [[My Github - github.com chuan-khuna]]

· Experience with data visualization tools is highly advantageous 

- [[Senior Project - Bias Correction of Multi-Dimensional Climate Data and Visualisation]]
- [[2020 - I am interested in...]]
- [[Analysis and Visualisation of Thailand Twitter IO Dataset]]

· Strong professional communication skills, including the ability to explain model design, risks, and outputs, and the ability to interface with executive leadership to translate complex analyses into action. 

**ADDITIONAL QUALIFICATIONS**:

· Familiar with Software development lifecycle (SDLC) and Test Driven Development (TDD). 

· Entrepreneurial, creative, curious, and inquisitive. 

· Ability to thrive in a fast-paced undefined and ambiguous environment. 

· Able to apply principles of the scientific method and empirical research. 

· Exceptional work ethic. 

 **Benefit:** 

 **·** **Location: BTS Chongnonsri and Work from home** 

· Flexible working hours 

· Health Insurance and Dental 

· Bonus 

· Happy Budget Benefit 

· Annual Outing 

· Bedroom & Bathroom 

· Snack & Coffee & Tea & Party