---
title:  SCB-DataScientist
aliases: []
tags: 
- job_description
---

orgainisation: [[SCB]]
position: [[Data Scientist]]
created: [[2021-07-12]]

https://careers.scb.co.th/th/jobs/areas/new-graduate/data-scientist

### About the job

**Job Summary**

As [[Data Scientist]] in [[SCB]], you will join a group of enthusiastic people working passionately to leverage this wealth of data for business uses. You will analyze the data, extract insights and create models for improving customer experience and formulating business strategies. You will work closely with multiple teams to identify opportunities and drive business results with data-driven research. Our team is constantly looking for people with strong quantitative and analytical backgrounds, who are excited about challenges at the intersection of data, math, coding, and machine learning.

  

**Job description:**

-   Apply statistical and [[machine learning]] methods to large, complex data sets to draw insights and provide actionable recommendations.
-   Solve complex problems on both technical and business sides using advanced analytical methods.
-   Work with Engineering teams to implement end-to-end process from model development to testing, validation, and deployment
-   Research and develop new quantitative models and frameworks to enhance the company’s data science capability

  

  

**Basic Qualifications :**

-   Bachelor’s degree in Engineering, Computer Science, Math, Physics, Statistics or other areas that are highly quantitative
	-	[[Apr 2020 - I graduated with a Bachelor in Comptuter Engineering from KMUTNB]]
-   Experience with statistical programming languages (e.g., [[Python]], [[R (Language)|R]] , [[pandas]]) and [[Database (Course)]] software (e.g., [[SQL]], PySpark)
	-   [[Apr 2020 - I graduated with a Bachelor in Comptuter Engineering from KMUTNB]]
	-   [[Artificial Intelligence (Course)|AI(course)]]
	-   [[Knowledge Discovery and Data Mining (Course)|Data Mining]]
	-   [[I have 4+ years of Python experience]]
-   Knowledge in statistics (e.g., hypothesis testing, regression) and [[machine learning]]
	-   [[Apr 2020 - I graduated with a Bachelor in Comptuter Engineering from KMUTNB]]
	-   [[Artificial Intelligence (Course)|AI(course)]]
	-   [[Knowledge Discovery and Data Mining (Course)|Data Mining]]
	-   [[Feb 2021 - I achieved TensorFlow Developer Certificate]]
	-   [[I implemented simple machine learning models]]
-   Strong analytical problem-solving capabilities

  

**Preferred Qualifications**

-   Master’s or PhD degree in a quantitative discipline
-   Experience applying [[machine learning]] and statistical methods to large datasets
-   Solid understanding of advanced statistics and machine learning practices.
	-   [[Senior Project - Bias Correction of Multi-Dimensional Climate Data and Visualisation]] (large data?)
	-   [[Feb 2021 - I achieved TensorFlow Developer Certificate]]
	-   [[Apr 2020 - I graduated with a Bachelor in Comptuter Engineering from KMUTNB]]
-   Experience in one or more specialized [[machine learning]] areas (e.g., [[Natural Language Processing|NLP]], deep learning, recommendation systems, reinforcement learning)
	-   [[Feb 2021 - I achieved TensorFlow Developer Certificate]]
	-   [[Neural Style Transfer]]
	-   [[Analysis and Visualisation of Thailand Twitter IO Dataset]]
	-   [[I implemented simple machine learning models]]
-   Outstanding coding skills or software development background.
	-   [[Apr 2020 - I graduated with a Bachelor in Comptuter Engineering from KMUTNB]]
	-   [[I have 4+ years of Python experience]]
-   Ability to think independently and communicate complex ideas to less technical persons
-   Excellent command of English in both verbal and written forms

### Contact the job poster
[[Tiwa Kumkaen]]