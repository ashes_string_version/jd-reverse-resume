---
title: BangkokBank - DataScientist
aliases: []
tags: [job_description]
---

orgainisation: [[Bangkok Bank]]
position: [[Data Scientist]]
last update:  [[2021-07-12]]

**Role & responsibilities:**

· Analyze customer-level data by applying data-mining techniques to better understand customers’ underlying needs and behavior and extract customer insights  

- [[Knowledge Discovery and Data Mining (Course)|Data Mining (course)]]

· Apply descriptive and predictive modeling techniques to extend the right offer for targeted campaign to enhance customer experience  

· Design and conduct analysis to support product & channel improvement and development  

· Present key findings and recommendations to business counter parties and senior management on customer relationship management approach and strategic planning  

· Participate in designing and maintaining Data Mart for supporting analytic work  

 **Qualifications:**

· Bachelor’s Degree or higher in Data Mining, Statistics, Engineering, Economics, IT or any related field  

- [[Apr 2020 - I graduated with a Bachelor in Comptuter Engineering from KMUTNB]]

· Thai nationality, aged 21 – 35  

- [[I was born in July 1997]]

· Experience in Data Analytics, Data Mining, Data Science will be an advantage  

- [[Knowledge Discovery and Data Mining (Course)|Data Mining (course)]]
- [[Artificial Intelligence (Course)]]
- [[Feb 2021 - I achieved TensorFlow Developer Certificate]]
- [[I implemented simple machine learning models]]
- [[Apr 2020-Dec 2020 - I achieved many online course certificates]]

 **Required Skills:**

-   Good analytical skills with business interest and mindset  
-   Good communication skills including ability to provide clear, and business-friendly explanation of complex analysis    

**For more information: K. Sitthi Tel. 0-2296-8173**

**E-Mail :** **Sitthi.preechaphuech@bangkokbank.com** 

**Apply job:** [https://icareers.bangkokbank.com/psc/ps/EMPLOYEE/HRMS/c/HRS_HRAM.HRS_CE.GBL?&](https://icareers.bangkokbank.com/psc/ps/EMPLOYEE/HRMS/c/HRS_HRAM.HRS_CE.GBL?&)