---
title: Wisesight-Machine Learning Engineering
aliases: []
tags: [job_description]
---

orgainisation: [[Wisesight]]
position: [[Machine Learning Engineer]]
last update: 
- [[2021-07-12]] data from 7 months ago 
- [[2021-07-18]] No significant difference

https://jobs.blognone.com/company/wisesight-hq-thailand/job/ai-developer-0OEd

**Responsibilities**

-   Working with large and complex datasets, comprising but not limited to, social media texts, images, and geo-locations.
	-   [[Senior Project - Bias Correction of Multi-Dimensional Climate Data and Visualisation]]
-   Developing and evaluating [[Artificial Intelligence|AI]] models for different use cases in continuous delivery environment.
	-   [[Artificial Intelligence (Course)]]
	-   [[Knowledge Discovery and Data Mining (Course)|Data Mining (course)]]
	-   [[I implemented simple machine learning models]]
	-   [[Feb 2021 - I achieved TensorFlow Developer Certificate]]
-   Designing new dataset and preparing data for new applications.
-   Extending and maintaining AI automation in the organization.

**Qualification**

-   Applicants from every training background are welcome (our team has people from anthropology, computer science, industrial engineering, and linguistics trainings).
	-   [[Apr 2020 - I graduated with a Bachelor in Comptuter Engineering from KMUTNB]]
	-   [[Senior Project - Bias Correction of Multi-Dimensional Climate Data and Visualisation]]
	-   [[Feb 2021 - I achieved TensorFlow Developer Certificate]]
-   [[Python]], basic regular expression, and basic [[SQL]] is preferred.
	-   [[Database (Course)]]
	-   [[I have 4+ years of Python experience]]
-   Fluently able to explain how your code/model is working and why you designed it this way.
	-   [[Feb 2021 - I achieved TensorFlow Developer Certificate]]
-   Experienced in at least one of the following areas: computer vision, information retrieval, natural language processing, and pattern recognition. Internship and self-directed learning projects can be counted.
	-   [[Feb 2021 - I achieved TensorFlow Developer Certificate]]
	-   [[2020 - I am interested in...]]
-   Optional: Experience with some of these data analysis libraries and machine learning architectures, like Caffe2, MXNet, [[pandas]], Scikit-learn, [[TensorFlow]], and Torch, is a plus.
	-   [[Senior Project - Bias Correction of Multi-Dimensional Climate Data and Visualisation]]
	-   [[Tensorflow Developer Certificate]]
-   Optional: Experience with Unix-based server and cloud platform management (AWS, Google Cloud) is a plus.
-   Optional: Experience in software development with virtual environment or container like Docker or Python’s venv is a plus.

**Some qualifications are missing? Fine! If you:**

-   Have a positive mindset.
-   Open for feedback.
-   Keen to self-managed and develop your own learning path.
	-   [[Apr 2020-Dec 2020 - I achieved many online course certificates]]

**-- we would like to hear your story**