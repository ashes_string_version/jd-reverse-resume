---
title: Senior Project - Bias Correction of Multi-Dimensional Climate Data and Visualisation
aliases: []
tags: [project]
---

# Senior Project - Bias Correction of Multi-Dimensional Climate Data and Visualisation

2019 - 2020

- the total size of Climate data is about 200GB-300GB
	- large?
	- it is large for my Laptop, 500GB HDD, 16GB of RAM 

- [[Python]]
	- [[pandas]], [[numpy]]	
- [[mongo DB]]
	- [[NoSQL]]
- [[R (Language)]] 
	- for advance Bias Correction Library
- [[visualisation]]
- [[climate trend analysis]]
	- [[Mann-Kendal test]], [[Theil-Sen Estimator]]
- [[bias correction]]