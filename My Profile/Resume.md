---
title: Resume
aliases: []
tags: []
---

# Resume

I have 1 year of experience in [[climate trend analysis]] and [[bias correction]], reduced the temperature bias from 3.8 to 1.9. Proficient in [[Python]], [[tensorflow]], data [[visualisation]]. Achieved [[Tensorflow Developer Certificate]]. Looking for an opportunity in [[data science]] and [[visualisation]] to broaden my knowledge.

## Experience and Project

I have no real working experience with any organisation. It is my self-proclaimed experience. It just represents what I have learnt, not what I can work in real situation.

- [[Senior Project - Bias Correction of Multi-Dimensional Climate Data and Visualisation]]
- [[Knowledge Discovery and Data Mining (Course)|Data Mining]] and [[Artificial Intelligence (Course)|AI(course)]]
	- [[I implemented simple machine learning models]]
- [[Neural Style Transfer]]
- [[Music Genre Classification Using Mel-Spectrogram]]
- [[Analysis and Visualisation of Thailand Twitter IO Dataset]]
- [[Simple POS Web Application]]

## Education

- [[Apr 2020 - I graduated with a Bachelor in Comptuter Engineering from KMUTNB]]
	- [[Bachelor in Computer Engineering, GPAX 3.83, 1st class honours]]

## Activity

- [[2019-2020 - Teching Assistant]]

## misc. / Skills

- [[I was born in July 1997]]
- [[I exempted from military service (completed ROTC)]]
- [[2020 - I am interested in...]]
- [[Apr 2020-Dec 2020 - I achieved many online course certificates]]
	- [[Jul 2020 - I achieved a certificate from Andrew Ng's Deep Learning Course(Coursera)]]
- [[Feb 2021 - I achieved TensorFlow Developer Certificate]]
- [[I have 4+ years of Python experience]]
