---
title: Apr 2020-Dec 2020 - I achieved many online course certificates
aliases: []
tags: [story]
---

# Apr 2020-Dec 2020 - I achieved many online course certificates

I was learning many things from online courses.

## Coursera:
- https://www.coursera.org/user/994e0ab7acdb7735d263db608e078d9c
- [[Jul 2020 - I achieved a certificate from Andrew Ng's Deep Learning Course(Coursera)]]

## DataCamp
- https://www.datacamp.com/profile/chuankhuna