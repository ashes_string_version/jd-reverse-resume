---
title: Apr 2020 - I graduated with a Bachelor in Comptuter Engineering from KMUTNB
aliases: []
tags: [story]
---

# Apr 2020 - I graduated with a Bachelor in Comptuter Engineering from KMUTNB

Aug 2016 - Apr 2020
	- [[Bachelor in Computer Engineering, GPAX 3.83, 1st class honours]]	
- [[Senior Project - Bias Correction of Multi-Dimensional Climate Data and Visualisation]]

## Enrolled Courses

- [[Artificial Intelligence (Course)|AI(course)]]
	- grade: A
- [[Knowledge Discovery and Data Mining (Course)|Data Mining]]
	- grade: A
- [[Database (Course)]]
	- [[SQL]], SQLite
	- grade: A
- [[Statistics for Computer Engineer]]
- Software Engineer
	- [[Ruby on Rails]]