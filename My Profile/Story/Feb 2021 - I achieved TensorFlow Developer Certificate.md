---
title: Feb 2021 - I achieved TensorFlow Developer Certificate
aliases: []
tags: [story, certificate]
---

# Feb 2021 - I achieved TensorFlow Developer Certificate

[[Tensorflow Developer Certificate]] 24/25
- https://www.credential.net/425e55ab-ed24-446a-a8bc-2c5b80622af2#gs.uidr12
- there are 5 sections, 5 points per section
	- 4/5 in time series prediction (RNN)