---
title: I implemented simple machine learning models
aliases: []
tags: [story]
---

# I implemented simple machine learning models

- I implemented simple machine learning models while I was learning [[Knowledge Discovery and Data Mining (Course)|Data Mining]] course.
	- [[Linear Regression]]
	- [[Logistic Regression]]
	- [[K-mean Clustering]] and Otsu method
	- [[Naive-Bayes]]
	- [[Bias-Variance Trade-off]]
	- bonus
		- I learnt about Genetic Algorithm in [[Artificial Intelligence (Course)|AI(course)]] and I successfully implemented simple GA to solve TSP and Knapsack problem in July 2020.
		- 