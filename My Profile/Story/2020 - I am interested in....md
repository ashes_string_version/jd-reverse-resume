---
title: 2020 - I am interested in...
aliases: []
tags: [story]
---

# 2020 - I am interested in...

background: 
- [[I implemented simple machine learning models]]
- [[I have 4+ years of Python experience]]
- [[Feb 2021 - I achieved TensorFlow Developer Certificate]]
- [[Apr 2020 - I graduated with a Bachelor in Comptuter Engineering from KMUTNB]]
- 

interesting:
- [[Python]]
- [[visualisation|Data Visualisation]]
	- I love visualise things by using [[Python]]
- after I learnt and achieved certificates ([[Apr 2020-Dec 2020 - I achieved many online course certificates]]) and [[I implemented simple machine learning models]]
	- I am interested in
		- [[Natural Language Processing|NLP]] > vision
		- number > images
		- [[Neural Style Transfer]], GANs